import os
import getpass

user = getpass.getuser()
root_loc="/home/"+user+"/Desktop/HTML,CSS,PHP/XHTML&CSS"

def list_files(startpath):
    for root, dirs, files in os.walk(startpath):
         level = root.replace(startpath, '').count(os.sep)
         tab = "\t" * (level)
         print('{}{}/'.format(tab, os.path.basename(root)))
         subindent = "\t" * (level + 1)
#         for f in files:
#            print('{}{}'.format(subindent, f))


list_files(root_loc)


#While Loops

i = 0
numbers = []

while i < 6:
    print "Appending %d" %i
    numbers.append(i)

    i +=1
    print "numbers now:", numbers
    print "At the bottom i is %d" %i

print "the numbers:"

for number in numbers:
    print number

a = 1+input("give me a number:")
list = []

for i in range(0,a):
    print "i=%d" %i
    list.append(i)

print list
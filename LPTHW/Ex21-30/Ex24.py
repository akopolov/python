#More Practice

print "some \' escapes \\ \n there are more thou "

poem = """
\tThe lovely world
with logic so firmly planted
cannot discern \n the needs of love
nor comprehend passion from intuition
and requires an explanation
\n\t\twhere there is none.
"""

print "_____"
print poem
print "_____"

five = 5+5-5*(5/5)

print "this shoulde be %d" %five

def formula(start):
    jelly_beans=start*50
    jars= jelly_beans/1000
    crates=jars/100
    return jelly_beans,jars,crates

start_point=10000

bens,jars,crates = formula(start_point)

print bens
print jars
print crates
#What Was That?

tabby_cat = "\tI am tabbed in."
persion_cat = "i am split \non a line"
backsplash_cat = "i am \\ a \\ cat"

fat_cat = """i will do a list:
\t* Cat food
\t* Fishes
\t* Catnip\n\t* Grass"""

print tabby_cat
print persion_cat
print backsplash_cat
print fat_cat

#while True:
for i in ["/","-","|","//","|"]:
    print "%r" %i,
    print "%s" %i

#NB!!!!!!!!!!
#%r prints as it is (raw output)
#%s print as if it would be a STRING
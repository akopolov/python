#Reading and Writing Files

from sys import  argv

script, filename =argv

print "we are going to erase %r" %filename
print "If you dont want that hit Ctrl-C (^C)"
print "If you do want that hit RETURN"

raw_input("?")

print "Opening the file..."
target = open(filename,'w')

print "Truncating(DELETING) the file(DATA).BB"
target.truncate()

print "Now i am going to ask for 3 lines"

line1 = raw_input("Line 1: ")
line2 = raw_input("Line 2: ")
line3 = raw_input("Line 3: ")

print "I am going to write the lines in the file"

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

print "and now we close it"

target.close()
#Names, Variables, Code, Functions

def print_two(*args):
    arg1, arg2 =args
    print "arg1: %s , arg2: %s" %(arg1, arg2)

def print_two_agen(arg1, arg2):
    print "arg1: %s , arg2: %s" %(arg1, arg2)

def print_one(arg1):
    print "arg1: %s" %arg1

def print_none():
    print "nothing"

print_two("1st","2nd")
print_two_agen("Cat","Dog")
print_one("Aleks")
print_none()




import os
import argparse
from datetime import datetime

parser = argparse.ArgumentParser(description='Apache2 log parser.')
parser.add_argument('--path',help='Path to Apache2 log',default="/var/log/apache2")
parser.add_argument('--top-urls',help="finde top URL-s",action='store_true')
parser.add_argument('--geoip',help="IP-s to count",action='store_true')
parser.add_argument('--verbose',help="IP-s to count",action='store_true')

args = parser.parse_args()
print "We are epecting logs from :", args.path
print "Do we want TOP URL-s\n",args.top_urls

files=[]

def humanize(bytes):
    if bytes<1024:
        return "%d B" %bytes
    elif bytes<1024**2:
        return "%d KB" %(bytes/1024)
    elif bytes<1024**3:
        return "%d MB" %(bytes/1024 ** 2)
    else:
        return "%d GB" %(bytes/1024 ** 3)

for filename in os.listdir(args.path):
    mode,inode,device,nlink,uid,gid,size,atime,mtime,ctime = os.stat(args.path+filename)
    files.append((filename, datetime.fromtimestamp(mtime),size))

files.sort(key = lambda(filename,dt,size):dt)

for filename, dt, size in files:
    print filename, "\t",dt,"\t",humanize(size)

print "newest file is :",files[-1][0]
print "Oldest file is :",files[0][0]
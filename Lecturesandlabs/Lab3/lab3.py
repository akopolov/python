#Python lab
import os
import operator
from collections import Counter

import argparse

parser = argparse.ArgumentParser(description='Apache2 log parser.')
parser.add_argument('--path',help='Path to Apache2 log',default="/var/log/apache2")
parser.add_argument('--top-urls',help="finde top URL-s",action='store_true')
parser.add_argument('--geoip',help="IP-s to count",action='store_true')
parser.add_argument('--verbose',help="IP-s to count",action='store_true')

args = parser.parse_args()
print "We are epecting logs from :", args.path
print "Do we want TOP URL-s\n",args.top_urls

total=0



for filename in os.listdir(args.path):

    if args.verbose:
        print "Parsing:",filename

    if not filename.startswith("access.log"):
        #print "Skipping unknown file:", filename
        continue
    if filename.endswith(".gz"):
       #print "Skipping compressed file:", filename
        continue
        #print "Going to process:", filename
    fh = open(os.path.join(args.path,filename))
    #keywords = "Windows", "Linux", "OS X", "Ubuntu", "Googlebot", "bingbot", "Android", "YandexBot", "facebookexternalhit"

    d = {}
    t = {}
    a = {}
    for line in fh:
        total+=1
        try:
            source_timestamp, request, response, referrer, _, agent, _ = line.split("\"")
            method, path, protocol = request.split(" ")
            if path.startswith("/~"):
                username,remainder = path[2:].split("/",1)
                ins,outs = response[1:].split(" ",1)
                if username in d:
                    d[username]+=1
                    t[username]+=int(ins)
                    a[username]+=int(outs)
                else:
                    d[username]=1
                    t[username]=int(ins)
                    a[username]=int(outs)
        except ValueError:
            pass

dic=dict(Counter(d).most_common(5))
ndic=sorted(dic.items(), key = lambda (keyword,hits):-hits)
for i in ndic:
    print i[0],"\t\t",i[1],"and the summ of outs:",a[i[0]]
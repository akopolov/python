import argparse

parser = argparse.ArgumentParser(description='Apache2 log parser.')
parser.add_argument('--path',help='Path to Apache2 log',default="/var/log/apache2")
parser.add_argument('--top-urls',help="finde top URL-s",action='store_true')
parser.add_argument('--geoip',help="IP-s to count",action='store_true')

args = parser.parse_args()
print "We are epecting logs from :", args.path
print "Do we want TOP URL-s",args.top_urls
import os
from flask import Flask
from jinja2 import Environment, FileSystemLoader
env = Environment(
    loader=FileSystemLoader(os.path.dirname(__file__)),
    trim_blocks=True)
app = Flask(__name__)

def list_log_files():
    for filename in os.listdir("/home/aleksei/logs"):
        if filename.endswith(".log"):
            yield filename
        if filename.endswith(".gz"):
            yield filename

def humanize(bytes):
    if bytes < 1024:
        return "%d B" % bytes
    elif bytes < 1024 ** 2:
        return "%.1f kB" % (bytes / 1024.0)
    elif bytes < 1024 ** 3:
        return "%.1f MB" % (bytes / 1024.0 ** 2)
    else:
        return "%.1f GB" % (bytes / 1024.0 ** 3)

@app.route("/report/")
def report():
    return env.get_template("report.html").render(
        humanize=humanize,
        user_bytes=(("whoever",500),))

@app.route("/")
def index():
    return env.get_template("index.html").render(
        log_files=list_log_files())

if __name__ == "__main__":
    app.run(debug=True,host="0.0.0.0")
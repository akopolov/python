#import GeoIP
#gi = GeoIP.open("/usr/share/GeoIP/GeoIP.dat", GeoIP.GEOIP_MEMORY_CACHE)
#print "Gotcha:", gi.country_code_by_addr("194.126.115.18").lower()

from lxml import etree
from lxml.cssselect import CSSSelector

document = etree.parse(open('BlankMap-World6.svg'))

sel = CSSSelector("#ee")
for j in sel(document):
    j.set("style", "fill:red")
    # Remove styling from children
    for i in j.iterfind("{http://www.w3.org/2000/svg}path"):
        i.attrib.pop("class", "")

with open("highlighted.svg", "w") as fh:
    fh.write(etree.tostring(document))
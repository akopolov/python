import os
import operator
from collections import Counter
import GeoIP
from lxml import etree
from lxml.cssselect import CSSSelector
gi = GeoIP.open("/usr/share/GeoIP/GeoIP.dat", GeoIP.GEOIP_MEMORY_CACHE)

root = "/home/aleks/logs/"
total=0
country={}

for filename in os.listdir(root):
    if not filename.startswith("access.log"):
        continue
    if filename.endswith(".gz"):
        continue

    fh = open(os.path.join(root,filename))
    ip_adresses={}
    for line in fh:
        total+=1
        source_timestamp, request, response, referrer, _, agent, _ = line.split("\"")
        #new
        ip_adress, _, _, timestamp, timezone, _ = source_timestamp.split(" ")
        if ":" not in ip_adress:
            ip_adresses[ip_adress]=ip_adresses.get(ip_adress,0)+1

for i in ip_adresses:#make a contry list with the cinty short name and the number of visiters
    cc=gi.country_code_by_addr(i)
    country.update({cc:ip_adresses[i]})

document = etree.parse(open('BlankMap-World6.svg'))
max_hits=max(country.values())
print country
print max_hits

for i in country:
    if i != None:
        sel = CSSSelector("#"+i.lower())
        for j in sel(document):
            j.set("style","fill:hsl(%d,100%%, 70%%);" % (120-country[i]*120/max_hits))
            for i in j.iterfind("{http://www.w3.org/2000/svg}path}"):
                i.attrib.pop("class","")

with open("highlighted.svg", "w") as fh:
    fh.write(etree.tostring(document))

print os.path.realpath("highlighted.svg")
os.system("x-www-browser file:///home/aleks/Desktop/Python/Lecturesandlabs/Lab5/highlighted.svg")
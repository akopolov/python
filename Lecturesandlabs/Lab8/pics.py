from PIL import Image
import os

d = "/home/lvosandi/images"         # This is the input folder
output = os.path.join(d, "smaller") # This is the output folder for small images
filenames = os.listdir(d)           # This is the list of files in the input folder

class ImageConverter(Thread): # ImageConverter shall be subclass of Thread
    def run(self): # It has run function which is run in a separate thread
        while True:
            try:
                filename = filenames.pop() # Try to get a filename from the list
            except IndexError:
                break
            if not filename.lower().endswith(".jpg"):
                continue
            print self.getName(), "is processing", filename
            im = Image.open(os.path.join(d, filename))
            width, height = im.size
            smaller = im.resize((800, height * 800 / width))
            smaller.save(os.path.join(output, filename))

if not os.path.exists(output):
    os.makedirs(output)

threads = []
for i in range(0, 8):
    threads.append(ImageConverter())
for thread in threads:
    thread.start() # Start up the threads
for thread in threads:
    thread.join() # Wait them to finish

#Python lab
import os
import operator
from collections import Counter

# Following is the directory with log files,
# On Windows substitute it where you downloaded the files
root = "/home/aleks/logs"
total=0


for filename in os.listdir(root):
    if not filename.startswith("access.log"):
        #print "Skipping unknown file:", filename
        continue
    if filename.endswith(".gz"):
       # print "Skipping compressed file:", filename
        continue
#    print "Going to process:", filename
    fh = open(os.path.join(root,filename))
    #keywords = "Windows", "Linux", "OS X", "Ubuntu", "Googlebot", "bingbot", "Android", "YandexBot", "facebookexternalhit"
    d = {}
    t = {}
    a = {}
    for line in fh:
        total+=1
        try:
            source_timestamp, request, response, referrer, _, agent, _ = line.split("\"")
            method, path, protocol = request.split(" ")
            if path.startswith("/~"):
                username,remainder = path[2:].split("/",1)
                ins,outs = response[1:].split(" ",1)
                if username in d:
                    d[username]+=1
                    t[username]+=int(ins)
                    a[username]+=int(outs)
                else:
                    d[username]=1
                    t[username]=int(ins)
                    a[username]=int(outs)
        except ValueError:
            pass
#    total = sum(d.values())
#    print "Total lines with requested keywords:", total
#    for keyword, hits in sorted(d.items(), key = lambda (keyword,hits):-hits):
#       print "%s => %d (%.02f%%)" % (keyword, hits, hits * 100 / total)

dic=dict(Counter(d).most_common(5))
ndic=sorted(dic.items(), key = lambda (keyword,hits):-hits)
for i in ndic:
    print i[0],"\t\t",i[1],"\tand the summ of outs:",a[i[0]]
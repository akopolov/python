#Python lab
import os
import pysftp
from collections import Counter

# Following is the directory with log files,
# On Windows substitute it where you downloaded the files
sftp = pysftp.Connection('',username='',password='')
#root = "/home/aleks/Desktop/Python/Lectures&labs/Lab2"
root = "/var/log/apache2"
sftp.cwd(root)

for filename in sftp.listdir(root):
    if not filename.startswith("access.log"):
        print "Skipping unknown file:", filename
        continue
    if filename.endswith(".gz"):
        print "Skipping compressed file:", filename
        continue
    print "Going to process:", filename
    fh = sftp.open(filename)
    keywords = "Windows", "Linux", "OS X", "Ubuntu", "Googlebot", "bingbot", "Android", "YandexBot", "facebookexternalhit"
    d = {}
    for line in fh:
        try:
            source_timestamp, request, response, referrer, _, agent, _ = line.split("\"")
            method, path, protocol = request.split(" ")
            for keyword in keywords:
                if keyword in agent:
                    d[keyword] = d.get(keyword, 0) + 1
                    break
        except ValueError:
            pass
    total = sum(d.values())
    print "Total lines with requested keywords:", total
    for keyword, hits in sorted(d.items(), key = lambda (keyword,hits):-hits):
        print "%s => %d (%.02f%%)" % (keyword, hits, hits * 100 / total)

sftp.close()

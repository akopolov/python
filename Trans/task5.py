import requests
web='http://bootcamp-api.transferwise.com'
key='?token=672d2e97e1eddf7f7616f4605d77aea2367a6261'
info='/task/5'
start='/task/5/start'
end='/task/finish'
payments='/payment'
exposed='/payment/{0}/aml'
exposed_list={}
requests.post(web+start+key)
for i in requests.get(web+info+key).json()['peps']:
    name=" ".join(i.split())
    name=name.split(" - ")
    exposed_list.update({name[0]:name[1]})
for customer in requests.get(web+payments+key).json():
    if (customer['recipientName'] in exposed_list) and (customer['recipientCountry']==exposed_list[customer['recipientName']]):
        print requests.put((web+exposed+key).format(customer['id'])).json()
    else:
        print requests.delete((web+exposed+key).format(customer['id'])).json()
print requests.post(web+end+key).json()
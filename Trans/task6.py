import requests
web='http://bootcamp-api.transferwise.com'
key='?token=672d2e97e1eddf7f7616f4605d77aea2367a6261'
info='/task/6'
start='/task/6/start'
history='/payment/history'
payments='/payment'
fraud='/payment/{0}/fraud'
end='/task/finish'
fraud_ip=[]
fraud_mail=[]
fraud_iban=[]
check_ip=[]
print requests.post(web+start+key).json()
for i in requests.get(web+history+key).json():
    if i['fraud']==True:
        if i['ip'] not in fraud_ip:
            fraud_ip.append(i['ip'])
        if i['email'] not in fraud_mail:
            fraud_mail.append(i['email'])
        if i['iban']not in fraud_iban:
            fraud_iban.append(i['iban'])
for x in range(len(fraud_ip)):
    for y in range(x+1,len(fraud_ip)):
        same=True
        ip=""
        for a,b in zip(fraud_ip[x].split('.'),fraud_ip[y].split('.')):
            if a==b and same:
                ip+=a+"."
            else:
                same=False
                if ip[:-1] not in check_ip:
                    check_ip.append(ip[:-1])
for payment in requests.get(web+payments+key).json():
    for ip in check_ip:
        not_send=True
        while not_send:
            if ((ip in payment['ip'])or(payment['email'] in fraud_mail)or((payment['iban'] in fraud_iban))):
                try:
                    status=requests.put((web+fraud+key).format(payment['id']))
                    print status.json()
                    if status.status_code==200:
                        not_send=False
                except requests.exceptions.RequestException:
                    continue
            else:
                try:
                    status=requests.delete((web+fraud+key).format(payment['id']))
                    print status.json()
                    if status.status_code==200:
                        not_send=False
                except requests.exceptions.RequestException:
                    continue
print requests.post(web+end+key).json()
import requests
import decimal
web='http://bootcamp-api.transferwise.com'
key='?token=672d2e97e1eddf7f7616f4605d77aea2367a6261'
start='/task/4/start'
currency='/currency'
company='/company'
quotes='/quote/{0}/{1}/{2}'
rate='/rate/midMarket/{0}/{1}'
send='/hiddenFee/forCompany'
end='/task/finish'
requests.post(web+start+key)
n=0
currency_list=requests.get(web+currency+key).json()
#currency_list=['USD','EUR']
for i in requests.get(web+company+key).json():
    company_list=requests.get(web+company+key).json()[i]
company_list=company_list.replace(',','')
company_list=company_list[1:-1].split()
amount_list=[100,1000,10000]
for amount in amount_list:
    for i in currency_list:
        for j in currency_list:
            if i != j:
                mass=requests.get((web+quotes+key).format(amount, i, j)).json()
                rate_mult=requests.get((web+rate+key).format(i, j)).json()['rate']
                fee_mult=requests.get((web+rate+key).format('GBP',i)).json()['rate']
                for comp in company_list:
                    a=amount*rate_mult
                    commission=mass[comp]['commissionPercentage']/100
                    fee=mass[comp]['feeInGbp'] * fee_mult
                    pay=(a/mass[comp]['offerRate'])+fee+(a/mass[comp]['offerRate'])*commission
                    c=(1-(mass[comp]['youPay']/pay))*100
                    #c=round((abs(a-pay)/((a+pay)/2))*100,1)
                    #fake_rate=(a/pay)
                    #c=(1-(fake_rate/rate_mult))*100
                    c=decimal.Decimal(c).quantize(decimal.Decimal('.1'),rounding=decimal.ROUND_HALF_EVEN)
                    not_send=True
                    while not_send:
                        try:
                            print amount,'\t',mass[comp]['youPay'],'\t',i,"to",j,'\t',mass[comp]['recipientReceives'],'\t',a,'\t',pay,'\t'
                            #print  (web+send+'/'+comp+'/'+str(amount)+'/'+i+'/'+j+'/'+str(c)+key)
                            status=requests.post(web+send+'/'+comp+'/'+str(amount)+'/'+i+'/'+j+'/'+ str(c)+key)
                            status.json()
                            if status.status_code==200:
                                not_send=False
                            n+=1
                        except:
                            continue
print requests.post(web+end+key).json()
print n
import requests
web = 'http://bootcamp-api.transferwise.com'
key = '?token=672d2e97e1eddf7f7616f4605d77aea2367a6261'
banks='/bank/'
accounts='/bankAccount'
payments='/payment'
start='/task/3/start'
end='/task/finish'
requests.post(web+start+key)
bank_list=requests.get(web+banks+key).json()
account_list=requests.get(web+accounts+key).json()
payment_list=requests.get(web+payments+key).json()
trans={"sBankName":"","sAccountNumber":"","tBankName":"","tAcountNumber":"","amount":""}
for payment in payment_list:
    trans['amount']=str(payment['amount'])
    trans['tAcountNumber']=str(payment['iban'])
    for account in account_list:
        if (account['accountName']=="TransferWise Ltd" and
        payment['targetCurrency']==account['currency'] and
        payment['recipientBankId']==account['bankId']):
            trans['sAccountNumber']=account['accountNumber']
    for bank in bank_list:
        if (payment['recipientBankId']==bank['id']):
            trans['sBankName']='%20'.join(str(bank['name']).split())
            trans['tBankName']='%20'.join(str(bank['name']).split())
    not_send=True
    while not_send:
        resp=requests.post(web+banks+trans['sBankName']+'/transfer/'+
                      trans['sAccountNumber']+'/'+trans['tBankName']+'/'+
                      trans['tAcountNumber']+'/'+trans['amount']+key)
        print resp
        if resp.status_code!=200:
            print ("Resending")
        else:
            not_send=False
print requests.post(web+end+key).json()
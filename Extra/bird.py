import os, sys
import pygame
from pygame.locals import *

def __main__():
    pygame.init()
    pipe = pygame.image.load("img/pipe.png")
    bird = pygame.image.load("img/bird.png")
    background = pygame.image.load("img/bg.png")
    screen = pygame.display.set_mode((640,480))
    pygame.display.set_caption('Floppy Bird')
    pygame.mouse.set_visible(0)
    print "pipe demetion:",pipe.get_size()
    print "back groud demention",background.get_size()
    level=[0,1,2,0,3,2,0,1,2,0,1,0]
    antipipe=pygame.transform.flip(pipe,0,1)
    frames = 0 #count frames
    vl=0
    vy=0
    y=0
    scene=pygame.Surface((320,240))
    while True:
        scene.fill(0)#this fills the buffer
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                return
            elif event.type==KEYDOWN and event.key == K_SPACE:
                vy-=5
                #print "space"
        if frames%100==0:
            vy+=1
            y+=vy

        for i in range(0,3):
            scene.blit(background,(148*i-(frames/10)%148,50),(0,0,200,300))
        frames+=1
        for offset,pipe_hieght in enumerate(level):
            if pipe_hieght==0:continue#skipe pippse that have a height of 0
            scene.blit(pipe,(400-frames/2+offset*32,180-pipe_hieght*32),(0,0,100,160))
            scene.blit(antipipe,(400-frames/2+offset*32,180-pipe_hieght*32),(0,0,100,160))
        scene.blit(bird,(100,100+y),(18*(int(frames/100)%3),0,18,13))
        pygame.transform.scale(scene,screen.get_size(),screen)
#        screen.blit(background, (50, 50), (0, 0, 200, 300))
#        screen.blit(bird, (100, 100), (0, 0, 17, 18))
        pygame.display.flip()

if __name__ == "__main__":
    __main__()
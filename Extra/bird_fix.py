import os, sys
import pygame
from pygame.locals import *
from random import  randint

def main_loop(screen):
    scoore=0
    pipe = pygame.image.load("img/pipe.png")
    bird = pygame.image.load("img/bird.png")
    background = pygame.image.load("img/bg.png")



    pygame.display.set_caption('Floppy Bird')
    pygame.mouse.set_visible(1)

    #print "Pipe dimensions:", pipe.get_size()
    #print "Background dimensions:", background.get_size()

    y = 100 #approxemetli in the center of the creen
    vy = 0


    level = [0,1,2,0,2,0,0,1,2,3,2,3,2,1,0,0,1,2,3,2,2,1,1,0,0,3,3]

    antipipe = pygame.transform.flip(pipe, 0, 1)
    frames = 0 # Here we count frames
    scene = pygame.Surface((240,180)) # This is where we compose the scene
    while True:
        scene.fill(0) # This fills the buffer with black pixels
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                return
            elif event.type == KEYDOWN and event.key == K_SPACE:
                vy -= 0.05
                print "Space was pressed!"

        vy += 0.0005
        y += vy

        width,heigh = scene.get_size()
        if y> heigh:
            print "Game Over,borders"
            return current_pipe
        elif y < 0:
            print "Game Over,borders"
            return current_pipe

        for i in range(0, 3):
            scene.blit(background, (i*148 - (frames/10) % 148, 0), (0, 0, 200, 300))
        frames += 1

        current_pipe=(frames/2-400+100)//32
        #level.append(randint(0,2))
        for offset, pipe_height in enumerate(level):
            if pipe_height == 0: continue # Skip pipes whose height is 0
            px=400-frames/2 + offset * 32
            py=180-pipe_height * 32
            scene.blit(pipe, (px, py), (0, 0, 100,160))
            scene.blit(antipipe, (px,py-250), (0, 0, 100,160))
            print "Px",px
            print "Frames",frames

            if offset==current_pipe:#highlight second pipe
                if y + 13> py:
                    print "Game Over,pipe"
                    return current_pipe
                    #pygame.draw.rect(scene,(255,0,0),(px,py,30,160))
                if y < py-90:
                    print "Game Over,pipe"
                    return current_pipe
                    #pygame.draw.rect(scene,(255,0,0),(px,py-250,30,160))

        if current_pipe > len(level):
            print "you have wone"
            return current_pipe

        scene.blit(bird, (100, int(y)), (18 * (int(frames/100) % 3), 0, 18, 13))
        pygame.transform.scale(scene, screen.get_size(), screen) # Here we scale it up and paste to screen
        pygame.display.flip()

def game_over(screen, score,font):
    text=font.render("Game Over! %d" %score ,True,(255,0,0))#this is expensive
    screen.blit(text,(400-text.get_width()//2, 300 - text.get_height()//2))
    while True:
        for event in pygame.event.get():
            if event.type==QUIT:
                return False
            elif event.type==KEYDOWN and event.key==K_ESCAPE:
                return False
            elif event.type==KEYDOWN and event.key==K_SPACE:
                return True
        pygame.display.flip()


def __main__():
    pygame.init()
    screen = pygame.display.set_mode((800,600))
    font = pygame.font.SysFont("sans",72)
    again = True
    while again:
        score = main_loop(screen)
        game_over(screen, score,font)
if __name__ == "__main__":
    __main__()
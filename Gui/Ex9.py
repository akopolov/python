from Tkinter import *

def Dosomething():
	print "some randomt text"

root=Tk()

#Main menu

a = Menu(root)
root.config(menu=a)

subMenu=Menu(a)
a.add_cascade(label="File",menu=subMenu)
subMenu.add_command(label="New project",command=Dosomething)
subMenu.add_command(label="New..",command=Dosomething)
subMenu.add_separator()
subMenu.add_command(label="Exit",command=Dosomething)

editMenu=Menu(a)
a.add_cascade(label="edit",menu=editMenu)
editMenu.add_command(label="Redo",command=Dosomething)

#******toolbar

toolbar=Frame(root,bg="blue")

b=Button(toolbar,text="insert image",command=Dosomething)
b.pack(side=LEFT,padx=2,pady=2)
printButton=Button(toolbar,text="print",command=Dosomething)
printButton.pack(side=LEFT,padx=2,pady=2)

toolbar.pack(side=TOP,fill=X)

root.mainloop()
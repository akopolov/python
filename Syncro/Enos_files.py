import pysftp
import datetime

server=''
user=''
password=''
enos_root="public_html/"
enos_folders={}
enos_files={}
enos_paths={}

def scann_enos(start,path,sftp):
    path_name=path.replace(start,"Start/")
    path_split=path.split("/")
    if not (any(item.startswith(".") for item in path_split)):
        path_time=sftp.stat(path).st_mtime
        a=datetime.datetime.fromtimestamp(path_time)
        for i in sftp.listdir(path):
            enos_paths.update({path_name:a})
            file_time=sftp.stat(path+i).st_mtime
            if sftp.isdir(path+i) and not i.startswith("."):
                if path_name not in enos_folders:
                    enos_folders.update({path_name:{i:file_time}})
                    scann_enos(start,path+i+"/",sftp)
                else:
                    enos_folders[path_name].update({i:file_time})
                    scann_enos(start,path+i+"/",sftp)
            elif not i.startswith("."):
                if path_name not in enos_files:
                    enos_files.update({path_name:{i:file_time}})
                else:
                    enos_files[path_name].update({i:file_time})


def enos_content(root,server,user,password):
    sftp=pysftp.Connection(server,username=user,password=password)
    path=sftp.pwd+"/"+root
    scann_enos(path,path,sftp)
    sftp.close()

    for i in enos_paths:
        print i,"\t",enos_paths[i]
        if i in enos_folders:
            for j in enos_folders[i]:
                print "\t Dir",j,"\t",enos_folders[i][j]
        if i in enos_files:
            for x in enos_files[i]:
                print "\t Files",x,"\t",enos_files[i][x]
















import os.path , time
import getpass
import datetime

user = getpass.getuser()
pickd_folder="HTML,CSS,PHP"+"/"
local_root= "/home/" + user + "/Desktop/"+pickd_folder

def local_content(root):
    local_files={}
    local_folders={}
    local_paths={}
    for path,direct,files in os.walk(root):
        path_name=path.replace("/home/"+user+"/Desktop/"+pickd_folder,"Start/")
        split_path= path.split("/")
        if not (any(item.startswith(".") for item in split_path)):
            for i in files:
                if not i.endswith("~"):
                    file_time=os.path.getmtime(path+"/"+i)
                    if path_name not in local_files:
                        local_files.update({path_name:{i:file_time}})
                    else:
                        local_files[path_name].update({i:file_time})
                else:
                    continue

            for j in direct:
                if j.startswith("."):
                    continue
                else:
                    folder_time=os.path.getmtime(path+"/"+j)
                    if path_name not in local_folders:
                        local_folders.update({path_name:{j:folder_time}})
                    else:
                        local_folders[path_name].update({j:folder_time})
            path_time=os.path.getmtime(path)
            local_paths.update({path_name:path_time})
        else:
            continue
    return local_files,local_folders,local_paths

files,folders,paths=local_content(local_root)

for i in paths:
    print i,"\t",paths[i]
    if i in folders:
        for j in folders[i]:
            print "\t Dir \t",j,"\t",folders[i][j]

    if i in files:
        for x in files[i]:
            print "\t File \t",x, "\t",files[i][x]

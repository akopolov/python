from Tkinter import *
import tkMessageBox
from main_window5 import run
import getpass
import pysftp

root = Tk()

def login(event):
    server_name=server.get()
    user_name=user.get()
    user_password=password.get()
    open_main_window=False

    try:
        sftp=pysftp.Connection(server.get(), user.get(), password=password.get())
        sftp.close()
        root.destroy()
        open_main_window=True
    except:
        tkMessageBox.showinfo("ERROR","Was not able to establish connection")

    if open_main_window:
        run("/home/" + getpass.getuser() + "/", server_name, user_name, user_password)

def hower_server(event):
    status.config(text="Tipe in the Servername (exaple: enos.itcollege.ee)")

def hower_user(event):
    status.config(text="Tipe in the Username")

def hower_pass(event):
    status.config(text="Tipe in the Password")

def hower_leave(event):
    status.config(text="")

root.geometry("350x200+375+275")
root.resizable(width=FALSE, height=FALSE)
root.title("Sync")
root.configure(background="white")
main_frame = Frame(root, bg="white")
main_frame.pack(fill=X, padx=10, pady=10)
lable1 = Label(main_frame, text="Server:", bg="white", font="12")
lable1.grid(row=0, sticky=E, pady=5)
lable2 = Label(main_frame, text="Username:", bg="white", font="12")
lable2.grid(row=1, sticky=E, pady=5)
lable3 = Label(main_frame, text="Password:", bg="white", font="12")
lable3.grid(row=2, sticky=E, pady=5)
server = Entry(main_frame, font="12")
server.grid(row=0, column=1)
user = Entry(main_frame, font="12")
user.grid(row=1, column=1)
password = Entry(main_frame, font="12",show="*")
password.grid(row=2, column=1)
button_frame = Frame(root, bg="white")
button_frame.pack(fill=X)
submit = Button(button_frame, text="LOGIN", font="12", command=lambda:login('a'))
submit.bind("<Return>",login)
submit.pack()
status = Label(root, text="", relief=SUNKEN, anchor=W)
status.pack(side=BOTTOM, fill=X)
server.bind("<Enter>", hower_server)
server.bind("<FocusIn>", hower_server)
server.bind("<FocusOut>", hower_leave)
server.bind("<Leave>", hower_leave)
user.bind("<Enter>",hower_user)
user.bind("<FocusIn>", hower_user)
user.bind("<Leave>", hower_leave)
password.bind("<Enter>",hower_pass)
password.bind("<FocusIn>",hower_pass)
password.bind("<FocusOut>",hower_leave)
password.bind("<Leave>", hower_leave)
root.mainloop()
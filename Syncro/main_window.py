from Tkinter import *
import time
import paramiko
import stat
#import tkFileDialog
from Connect import server_connect
import os

def run (start):
    root=Tk()

    root.geometry("500x300+375+275")
    #root.resizable(width=FALSE, height=FALSE)
    root.title("Sync")
    root.configure(background="white")

    header_loc=Frame(root,bg='white',height=2, bd=1, relief=SUNKEN)
    header_loc.pack(fill="both")

    local_frame=Frame(root,bg='white',height=2, bd=1, relief=SUNKEN)
    local_frame.pack(fill="both")
    display_loc(start,local_frame,root,header_loc)

    server_frame=Frame(root,bg='white')
    server_frame.pack(fill=X)
    #display_server("")

    root.mainloop()

def open_dir(direct,root,start,header_loc,frame):
    for widget in frame.winfo_children():
        widget.destroy()
    for widget in header_loc.winfo_children():
        widget.destroy()
    display_loc(start+direct+"/",frame,root,header_loc)

def display_loc(path_l,frame,root,header_loc):
    name = path_l.split("/")
    Label(header_loc, text="Local Dir: " + name[len(name) - 2], bg='white', font='10').pack(fill="none", pady=5)

    j = 0
    for i in os.listdir(path_l):
        if not i.lower().startswith('.') and os.path.isdir(path_l + i):
            label_list=Label(frame, text=i, bg='white', font='10')
            label_list.grid(row=j, column=0,sticky=E, pady=5, padx=5)
            label_list.bind("<Button-1>", lambda e,i=i:open_dir(i,root,path_l,header_loc,frame))
            frame.bind("<Button-3>", lambda e, i=i: open_dir("", root, path_l[:-len(name[len(name)-2])-2], header_loc, frame))
            label_list.bind("<Button-3>", lambda e, i=i: open_dir("", root, path_l[:-len(name[len(name)-2])-2], header_loc, frame))
            label_list=Label(frame,text=time.ctime(os.path.getmtime(path_l+i)),bg='white', font='10')
            label_list.grid(row=j, column=1,sticky=W, pady=5, padx=30)
            label_list.bind("<Button-1>", lambda e,i=i:open_dir(i,root,path_l,header_loc,frame))
            j+=1

    # scrollbar.config(command=scrolframe.yview)

def display_server(move):
    sftpURL = server
    sftpUser = name
    sftpPass = password

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(sftpURL, username=sftpUser, password=sftpPass)
    ftp = ssh.open_sftp()
    for file in ftp.listdir_attr():
        if not file.startswith('.') or stat.S_ISDIR(file.st_mode):
            print file

    ftp.close()

run('/home/aleksei/')


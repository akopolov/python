import os, stat
import pysftp
import getpass
import time
import datetime

server_folders={}
server_files={}
server_paths={}
bad_names=["~","#","%","&","*","{","}","\\","/",":",";","<",">","?","|","\"","."]

def permissions_to_unix_name(st):
    is_dir = 'd' if stat.S_ISDIR(st.st_mode) else '-'
    dic = {'7':'rwx', '6' :'rw-', '5' : 'r-x', '4':'r--', '0': '---'}
    perm = str(oct(st.st_mode)[-3:])
    return is_dir + ''.join(dic.get(x,x) for x in perm)

def scann_server(start,path,sftp):
    path_name=path.replace(start,"Start/")
    path_split=path.split("/")
    if not (any(item.startswith(".") for item in path_split)):
        path_time=sftp.stat(path).st_mtime
        server_paths.update({path_name:path_time})
        for i in sftp.listdir(path):
            file_time=sftp.stat(path+i).st_mtime
            if sftp.isdir(path+i) and not i.startswith("."):
                if path_name not in server_folders:
                    server_folders.update({path_name:{i:file_time}})
                    scann_server(start,path+i+"/",sftp)
                else:
                    server_folders[path_name].update({i:file_time})
                    scann_server(start,path+i+"/",sftp)
            elif not i.startswith("."):
                if path_name not in server_files:
                    server_files.update({path_name:{i:file_time}})
                else:
                    server_files[path_name].update({i:file_time})
    return server_paths,server_folders,server_files

def server_root_pick(sftp):
    path = sftp.pwd+"/"
    print "\n"
    for i in sftp.listdir_attr(path):
        #TODO tryto fide if you can print only dirs
        print i
    move=""
    print "Pick a folder or make a new one"
    print "Press 'ENTER' to start the sync in the current folder"
    print "Type 'exit' to leave"
    move=raw_input(path+">>:")
    if move.lower()=="exit":
        if exit_check():
            return None
        else:
            return server_root_pick(sftp)
    elif move in sftp.listdir() and not sftp.isdir(path+move):
        print "This is not a folder pleas pick a folder or make a new one"
        #time.sleep(2)
        return server_root_pick(sftp)
    elif move in sftp.listdir() and sftp.isdir(path+move):
        check=""
        while len(check)==0:
            print "A file with this name exists. Move to it?(Y/N)"
            check=raw_input(path+">>:")
            if check.lower()=="y":
                sftp.chdir(path+move)
                return server_root_pick(sftp)
            elif check.lower()=="n":
                return server_root_pick(sftp)
            else:
                check=""
    elif move not in sftp.listdir() and any(i in move for i in bad_names ):
        print "\nYou are using a symbol in the name of a folder that is not recomendet to use"
        print "Here are all the sumbols that should not be in a folder name"
        print bad_names
        #time.sleep(3)
        return server_root_pick(sftp)
    elif move=="":
        check=""
        while len(check)==0:
            print "start Sycn here:(Y/N)?:"+path
            check=raw_input(path+">>:")
            if check.lower()=="y":
                return sftp.pwd
            elif check.lower()=="n":
                return server_root_pick(sftp)
            else:
                check=""
    elif move not in sftp.listdir():
        check=""
        while len(check)==0:
            print "\nMaking a new folder(Y/N)?:"+move
            check=raw_input(path+">>:")
            if check.lower()=="y":
                sftp.mkdir(path+"/"+move)
                return  server_root_pick(sftp)
            elif check.lower()=="n":
                return server_root_pick(sftp)
            else:
                check=""

def server_connect(server,user,password):
    sftp=pysftp.Connection(server,username=user,password=password)
    print "\nConnected\n"
    #TODO check all parts of the code
    #path=server_root_pick(sftp)+"/"
    path="/home/akopolov/public_html/"
    s_path,s_folders,s_files=scann_server(path,path,sftp)
    sftp.close()
    return s_path,s_folders,s_files

def local_root_pick(l_path):
    print "\n"
    for i in os.listdir(l_path):
        if os.path.isdir(l_path+i) and not (i.startswith(".") or i.endswith("~")):
            info=os.stat(l_path+i)
            m_time=int(info.st_mtime)
            permission=permissions_to_unix_name(info)
            print permission,"\t",datetime.datetime.fromtimestamp(m_time),"\t",i
    print"Pick a folder on the local mashine OR make a new one"
    print"'Exit' to leave"
    move=raw_input(l_path+">>:")
    if move.lower()=="exit":
        if exit_check():
            return None
        else:
            return local_root_pick(l_path)
    elif any(i in move for i in bad_names):
        print "\nPlease make shore that you are not using any of the folloing sumbols"
        print bad_names
        time.sleep(2)
        return local_root_pick(l_path)
    elif move == "":
        check=""
        while check=="":
            print"\nSync here(Y/N)?"
            check=raw_input(l_path+">>:")
            if check.lower()=="y":
                return l_path
            elif check.lower()=="n":
                return local_root_pick(l_path)
            else:
                check=""
    elif move not in os.listdir(l_path) and not any(i in move for i in bad_names):
        print "make a new folder '"+move+"'(Y/N)?"
        check=""
        while check =="":
            check=raw_input(l_path+">>:")
            if check.lower()=="y":
                os.mkdir(l_path+move)
                return local_root_pick(l_path+move+"/")
            elif check.lower()=="n":
                return local_root_pick(l_path)
            else:
                check=""
    elif move in os.listdir(l_path) and not any(i in move for i in bad_names):
        check=""
        while check=="":
            print"\na folder with such name exists would you like to move to it?(Y/N)"
            check=raw_input(l_path+">>:")
            if check.lower()=="y":
                return local_root_pick(l_path+move+"/")
            elif check.lower()=="n":
                return local_root_pick(l_path)
            else:
                check=""

def local_content():
    #pickd_folder=local_root_pick("/home/"+getpass.getuser()+"/")
    pickd_folder="/home/aleksei/Desktop/html-css-php/"
    local_files={}
    local_folders={}
    local_paths={}
    for path,direct,files in os.walk(pickd_folder):
        path_name=path.replace(pickd_folder,"Start/")
        if path_name != "Start/":
            path_name=path_name+"/"
        split_path=path.split("/")
        if not (any(item.startswith(".") for item in split_path)):
            for i in files:
                if not i.endswith("~"):
                    file_time=int(os.path.getmtime(path+"/"+i))
                    if path_name not in local_files:
                        local_files.update({path_name:{i:file_time}})
                    else:
                        local_files[path_name].update({i:file_time})
                else:
                    continue
            for j in direct:
                if j.startswith("."):
                    continue
                else:
                    folder_time=int(os.path.getmtime(path+"/"+j))
                    if path_name not in local_folders:
                        local_folders.update({path_name:{j:folder_time}})
                    else:
                        local_folders[path_name].update({j:folder_time})
            path_time=int(os.path.getmtime(path))
            local_paths.update({path_name:path_time})
        else:
            continue
    return local_paths,local_folders,local_files

def exit_check():
    out=""
    while len(out)==0:
        print "Are you shore you want to leave?(Y/N)"
        out=raw_input(">>:")
        if out.lower()=="y":
            return True
        elif out.lower()=="n":
            return False
        else:
            out=""

def compare(s_paths,s_folders,s_files,l_paths,l_folders,l_files):
    print "\nSERVER"
    for i in s_paths:
        print i
    print "\nLocal"
    for j in l_paths:
        print j

def start():
    password=""
    server=""
    login=""
    print "\nHello you are using Sync"
    print "Type 'exit' if you want to leave"
    while len(server)==0:
        print "\nPleas give me the name of a server"
        server=raw_input(">>:")
        if server.lower()=="exit":
            if exit_check():
                return None
            else:
                server=""
    while len(login)==0:
        print "\nPleas give login"
        login=raw_input(">>:")
        if login.lower()=="exit":
            if exit_check():
                return None
            else:
                login=""
    while len(password)==0:
        print "\nPlea give password"
        print "NB you will not see the input text"
        password=getpass.getpass(">>:")
        if password.lower()=="exit":
            if exit_check():
                return None
            else:
                password=""
    try:
        pysftp.Connection(server,username=login,password=password)
        print "Connecting"
        s_path,s_folders,s_files=server_connect(server,login,password)
        l_path,l_folders,l_files=local_content()
        compare(s_path,s_folders,s_files,l_path,l_folders,l_files)
    except:
        print "\nWas not able to connecto to a server"
        print "Chekc your login,password and servername"
        print "Server:\t",server
        print "Login:\t",login
        return start()
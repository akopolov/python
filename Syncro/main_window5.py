from Tkinter import *
import os
import paramiko
from datetime import datetime as dt
import stat

loc_path=""
server_files = {}
server_paths = {}
local_files = {}
local_paths = {}
loc_frame_name=""
loc_frame_time=""
loc_header_label=""
serv_frame_name=""
serv_frame_time=""
serv_header_label=""

def mkdir_p(sftp, remote_directory):
    dir_path = str()
    for dir_folder in remote_directory.split("/"):
        if dir_folder == "":
            continue
        dir_path += r"/{0}".format(dir_folder)
        try:
            sftp.listdir(dir_path)
        except IOError:
            sftp.mkdir(dir_path)

def compare(path,sftp):
    frame_list = [loc_frame_name, loc_frame_time, serv_frame_name, serv_frame_time]

    for item_loc in local_paths:
        new_serv_path = item_loc.split('/')
        del new_serv_path[0]
        temp_path = '/'.join(new_serv_path)
        new_serv_path = sftp.getcwd() + '/' + '/'.join(new_serv_path)
        if item_loc not in server_paths:
            mkdir_p(sftp,new_serv_path)
            if item_loc in local_files:
                for fl in local_files[item_loc]:
                    if fl != None:
                        sftp.put(path + temp_path + fl, new_serv_path + fl)
        elif item_loc in server_paths and item_loc in local_files:
            for chek_item in local_files[item_loc]:
                if chek_item != None:
                    if chek_item not in server_files[item_loc]:
                        sftp.put(path + temp_path + chek_item, new_serv_path + chek_item)
                    elif chek_item in server_files[item_loc]:
                        if local_files[item_loc][chek_item]>server_files[item_loc][chek_item]:
                            sftp.put(path + temp_path + chek_item, new_serv_path + chek_item)

    for item_serv in server_paths:
        new_loc_path = item_serv.split('/')
        del new_loc_path[0]
        new_serv_path = sftp.getcwd() + '/' + '/'.join(new_loc_path)
        new_loc_path=path+'/'.join(new_loc_path)
        if item_serv not in local_paths:
            if not os.path.isdir(new_loc_path):
               os.makedirs(new_loc_path)
            if item_serv in server_files:
                for fl in server_files[item_serv]:
                    if fl != None:
                        sftp.get(new_serv_path + fl, new_loc_path + fl)
        elif item_serv in local_paths and item_serv in server_files:
            for chek_item in server_files[item_serv]:
                if chek_item != None:
                    if chek_item not in local_files[item_serv]:
                        sftp.get(new_serv_path+chek_item, new_loc_path+chek_item)
                    elif chek_item in local_files[item_serv]:
                        if server_files[item_serv][chek_item]>local_files[item_serv][chek_item]:
                            sftp.get(new_serv_path + chek_item, new_loc_path + chek_item)

    for frame in frame_list:
        n=0
        for widget in frame.winfo_children():
            if n != 0:
                widget.destroy()
            else:
                n = 1
    show_loc_dir(path, loc_frame_name, loc_frame_time, loc_header_label)
    show_serv_dir(sftp, serv_frame_name, serv_frame_time, serv_header_label)

def scann_server(start,path,sftp):
    global server_paths
    global server_files
    path_name=path.replace(start,"Start") + '/'
    path_time=sftp.stat(path).st_mtime
    server_paths.update({path_name: path_time})
    for item in sftp.listdir_attr(path):
        if (not item.filename.startswith(".")) and (not item.filename.endswith("~")):
            file_time = sftp.stat(path+'/'+item.filename).st_mtime
            if stat.S_ISDIR(item.st_mode):
                scann_server(start, path + "/" + item.filename, sftp)
            elif not item.filename.startswith("."):
                if path_name not in server_files:
                    server_files.update({path_name: {item.filename: file_time}})
                else:
                    server_files[path_name].update({item.filename: file_time})

def local_content(pickd_folder):
    global local_files
    global local_paths
    for path, direct, files in os.walk(pickd_folder):
        path_name=path.replace(pickd_folder,"Start/")
        if path_name != "Start/":
            path_name=path_name+"/"
        split_path=path.split("/")
        if not (any(item.startswith(".") for item in split_path)):
            local_files.update({path_name: {None:None}})
            for i in files:
                if not i.endswith("~"):
                    file_time=int(os.path.getmtime(path+"/"+i))
                    # if path_name not in local_files:
                    #     local_files.update({path_name:{i:file_time}})
                    # else:
                    local_files[path_name].update({i:file_time})
                else:
                    continue
            path_time=int(os.path.getmtime(path))
            local_paths.update({path_name:path_time})
        else:
            continue

def sync(path,sftp):
    global local_files
    global local_paths
    global server_paths
    global server_files
    server_files = {}
    server_paths = {}
    local_files = {}
    local_paths = {}
    scann_server(sftp.getcwd(),sftp.getcwd(),sftp)
    local_content(path)
    compare(path,sftp)

def leave(root,sftp):
    sftp.close()
    root.destroy()

def move_serv(sftp, name, frame_name, frame_time, serv_head):
    n=0
    if name=="":
        temp=sftp.getcwd().split('/')
        del temp[-1]
        name='/'.join(temp)
        sftp.chdir(name)
    else:
        sftp.chdir(sftp.getcwd()+'/'+name)
        name=sftp.getcwd().split('/')
    for widget in frame_name.winfo_children():
         if n!=0:
             widget.destroy()
         else:
             n=1
    n=0
    for widget in frame_time.winfo_children():
        if n != 0:
            widget.destroy()
        else:
            n = 1
    show_serv_dir(sftp, frame_name, frame_time, serv_head)

def show_serv_dir(sftp,frame_name,frame_time,serv_head):
    if sftp.getcwd()==None:
        sftp.chdir('.')
    name=sftp.getcwd().split('/')
    serv_head.config(text="Server Dir: " + name[len(name)-1])
    frame_name.bind("<Button-3>", lambda e, i="": move_serv(sftp, i, frame_name, frame_time, serv_head))
    frame_time.bind("<Button-3>", lambda e, i="": move_serv(sftp, i, frame_name, frame_time, serv_head))
    for item in sftp.listdir_attr():
        if not item.filename.startswith('.'):
            label = Label(frame_name, text=item.filename, bg='white', font='10', anchor=W)
            label.pack(fill=X, pady=5, padx=5)
            if stat.S_ISDIR(item.st_mode):
                label.bind("<Button-1>", lambda e, i=item.filename: move_serv(sftp, i, frame_name, frame_time, serv_head))
            label.bind("<Button-3>", lambda e, i="": move_serv(sftp, i, frame_name, frame_time, serv_head))
            utime = sftp.stat(sftp.getcwd()+'/'+item.filename).st_mtime
            last_modified = dt.fromtimestamp(utime)
            label=Label(frame_time,text=last_modified,bg='white', font='10',anchor=W)
            label.pack(fill=X,pady=5, padx=5)
            if stat.S_ISDIR(item.st_mode):
                label.bind("<Button-1>", lambda e, i=item.filename: move_serv(sftp, i, frame_name, frame_time, serv_head))
            label.bind("<Button-3>", lambda e, i="": move_serv(sftp, i, frame_name, frame_time, serv_head))

def move_loc(path, name, frame_name, frame_time, loc_head):
    n = 0
    new = path+name+'/'
    for widget in frame_name.winfo_children():
         if n != 0:
             widget.destroy()
         else:
             n = 1
    n = 0
    for widget in frame_time.winfo_children():
        if n != 0:
            widget.destroy()
        else:
            n = 1
    return show_loc_dir(new, frame_name, frame_time, loc_head)

def show_loc_dir(path, frame_name, frame_time, loc_head):
    name = path.split('/')
    global loc_path
    loc_path = path
    loc_head.config(text="Local Dir: "+name[len(name)-2])
    frame_name.bind("<Button-3>", lambda e, i="": move_loc(path[:-len(name[len(name) - 2]) - 2], i, frame_name, frame_time, loc_head))
    frame_time.bind("<Button-3>", lambda e, i="": move_loc(path[:-len(name[len(name) - 2]) - 2], i, frame_name, frame_time, loc_head))
    for item in os.listdir(path):
        if not item.lower().startswith('.') and not item.lower().endswith('~'):
            label = Label(frame_name, text=item, bg='white', font='10',anchor=W)
            label.pack(fill=X, pady=5, padx=5)
            if os.path.isdir(path + item):
                label.bind("<Button-1>", lambda e, i=item: move_loc(path, i, frame_name, frame_time, loc_head))
            label.bind("<Button-3>", lambda e, i=item: move_loc(path[:-len(name[len(name) - 2]) - 2], "", frame_name, frame_time, loc_head))
            utime = os.path.getmtime(path+item)
            last_modified = dt.fromtimestamp(utime).strftime("%Y-%m-%d %H:%M:%S")
            label = Label(frame_time, text=last_modified, bg='white', font='10', anchor=W)
            label.pack(fill=X, pady=5, padx=5)
            if os.path.isdir(path + item):
                label.bind("<Button-1>", lambda e, i=item: move_loc(path, i, frame_name, frame_time, loc_head))
            label.bind("<Button-3>", lambda e, i=item: move_loc(path[:-len(name[len(name) - 2]) - 2], "", frame_name, frame_time, loc_head))

def run(path, server_name, user_name, user_password):
    global loc_frame_name, loc_frame_time, loc_header_label, serv_frame_name, serv_frame_time, serv_header_label
    root = Tk()
    root.resizable(width=FALSE, height=FALSE)
    root.title("Sync")
    root.configure(background="white")
    header = Frame(root, bg='white', borderwidth=1)
    header.pack(side=TOP, fill=X)
    header_label = Label(header, text="header", bg='white', font='10')
    header_label.pack(side=TOP, fill=BOTH, pady=5, padx=5)
    body = Frame(root, bg='white')
    body.pack(side=TOP, fill=BOTH, expand=YES)
    footer = Frame(root, borderwidth=1, relief=RAISED)
    footer.pack(side=BOTTOM, fill=X)
    close_button = Button(footer, text="Close", font="12")
    close_button.pack(side=RIGHT, padx=5, pady=5)
    ok_button = Button(footer, text="OK", font="12")
    ok_button.pack(side=RIGHT)
    loc_frame = Frame(body, bg='white', bd=1, relief=GROOVE)
    loc_frame.pack(fill=BOTH, side=LEFT, expand=YES)
    serv_frame = Frame(body, bg='white', bd=1, relief=GROOVE)
    serv_frame.pack(fill=BOTH, side=LEFT, expand=YES)
    loc_frame_header = Frame(loc_frame, bg="white", bd=1, relief=GROOVE)
    loc_frame_header.pack(fill=X)
    loc_frame_body = Frame(loc_frame, bg="white")
    loc_frame_body.pack(fill=BOTH, expand=YES)
    serv_frame_header = Frame(serv_frame, bg="white", bd=1, relief=GROOVE)
    serv_frame_header.pack(fill=X)
    serv_frame_body = Frame(serv_frame, bg="white")
    serv_frame_body.pack(fill=BOTH, expand=YES)
    loc_header_label = Label(loc_frame_header, text="Local Dir: ", bg='white', font='10')
    loc_header_label.pack(side=TOP, fill=X, pady=5, padx=5)
    serv_header_label = Label(serv_frame_header, text="Server Dir:", bg='white', font='10')
    serv_header_label.pack(side=TOP, fill=X, pady=5, padx=5)
    loc_frame_name = Frame(loc_frame_body, bg='white', bd=1, relief=GROOVE)
    loc_frame_name.pack(side=LEFT, fill=BOTH, expand=YES)
    loc_frame_time = Frame(loc_frame_body, bg='white', bd=1, relief=GROOVE)
    loc_frame_time.pack(side=LEFT, fill=BOTH, expand=YES)
    Label(loc_frame_name, text="Items", bg='white', font='10', bd=1, relief=GROOVE).pack(fill=X)
    Label(loc_frame_time, text="Time", bg='white', font='10', bd=1, relief=GROOVE).pack(fill=X)
    serv_frame_name = Frame(serv_frame_body, bg='white', bd=1, relief=GROOVE)
    serv_frame_name.pack(side=LEFT, fill=BOTH, expand=YES)
    serv_frame_time = Frame(serv_frame_body, bg='white', bd=1, relief=GROOVE)
    serv_frame_time.pack(side=LEFT, fill=BOTH, expand=YES)
    Label(serv_frame_name, text="Items", bg='white', font='10', bd=1, relief=GROOVE).pack(fill=X)
    Label(serv_frame_time, text="Time", bg='white', font='10', bd=1, relief=GROOVE).pack(fill=X)

    show_loc_dir(path, loc_frame_name, loc_frame_time, loc_header_label)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server_name, username=user_name, password=user_password)
    sftp = ssh.open_sftp()

    show_serv_dir(sftp, serv_frame_name, serv_frame_time, serv_header_label)

    close_button.config(command=lambda: leave(root, sftp))

    ok_button.config(command=lambda: sync(loc_path, sftp))

    root.mainloop()
